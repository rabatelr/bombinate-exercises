import { getObjectsFromArrayWithUniqueChosenField } from './getUniqueObjects';


describe('getUniqueObjects', () => {
    test('should remove duplicates (items non unique by chosen field value)', () => {
        const dataArrayWithDuplicatesValues = [
            { id: 1, name: "paint" },
            { id: 2, name: "bead" },
            { id: 3, name: "arm" },
            { id: 4, name: "snakes" },
            { id: 5, name: "wire" },
            { id: 6, name: "ducks" },
            { id: 7, name: "ducks" },
            { id: 1, name: "cork" },
            { id: 1, name: "bed" }
        ];

        // with unique id field value 
        let resultArray = getObjectsFromArrayWithUniqueChosenField(dataArrayWithDuplicatesValues);
        expect(resultArray).toEqual([
            { id: 1, name: "paint" },
            { id: 2, name: "bead" },
            { id: 3, name: "arm" },
            { id: 4, name: "snakes" },
            { id: 5, name: "wire" },
            { id: 6, name: "ducks" },
            { id: 7, name: "ducks" },
        ])

        // with unique otherField value
        resultArray = getObjectsFromArrayWithUniqueChosenField(dataArrayWithDuplicatesValues, 'name');
        expect(resultArray).toEqual([
            { id: 1, name: "paint" },
            { id: 2, name: "bead" },
            { id: 3, name: "arm" },
            { id: 4, name: "snakes" },
            { id: 5, name: "wire" },
            { id: 6, name: "ducks" },
            { id: 1, name: "cork" },
            { id: 1, name: "bed" }
        ])
    })
});