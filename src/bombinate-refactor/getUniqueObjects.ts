export const getObjectsFromArrayWithUniqueChosenField = (sourceArray: any[], fieldWhoseValueHasToBeUnique = "id") => {
  return sourceArray.reduce((accumulatedResult, currentItemInArray) => {
    const duplicateItem = getItemFromArrayWithSameFieldValue(accumulatedResult, fieldWhoseValueHasToBeUnique, currentItemInArray);
    if (!duplicateItem)
      return accumulatedResult.concat(currentItemInArray);
    return accumulatedResult;
  }, []);
};
function getItemFromArrayWithSameFieldValue(arrayToSearch: any[], fieldName: string, itemToCompare: any) {
  return arrayToSearch.find(item => item[fieldName] === itemToCompare[fieldName]);
}

