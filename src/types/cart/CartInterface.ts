import { Sku } from "../product/Product";

export interface CartInterface {
    linesInCart: ProductLineInCart[];
    addSkuToCart(Sku: Sku): void;
    removeAllSkusFromCart(): void;
    removeSkuFromCart(skuId: string, quantity?: number): void;
    increaseQuantityOfSkuById(skuId: string, amount?: number): void;
    decreaseQuantityOfSkuById(skuId: string, amount?: number): void;
    getTotalPrice(): number;
    isSkuAlreadyInCart(skuId: string): boolean;
    findLineInCartBySkuId(skuId: string): ProductLineInCart | undefined;
}

export type ProductLineInCart = {
    sku: Sku;
    quantity: number;
    price: number;
}