export type Product = {
    image: string;
    brand: {
        id: string;
        name: string;
        slug: string;
    }
    title: string;
    id: string;
    skus: Sku[]
}
export type Sku = {
    description: string;
    id: string;
    price: number;
    stock: number;
}

export type brand = {
    id: string;
    name: string;
    slug: string;
}