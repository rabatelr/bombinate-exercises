import { Product } from "../product/Product";

export interface CatalogueInterface {
    productsList: Product[];
    getProduct(idProduct:string):Product | undefined;
}

