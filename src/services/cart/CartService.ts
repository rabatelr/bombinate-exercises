import { CartInterface, ProductLineInCart } from "../../types/cart/CartInterface";
import { Sku } from "../../types/product/Product";
import currency from 'currency.js';
export class CartService implements CartInterface {
    linesInCart: ProductLineInCart[];


    constructor() {
        this.linesInCart = [];
    }

    addSkuToCart(sku: Sku) {
        if (this.isSkuAlreadyInCart(sku.id)) {
            this.increaseQuantityOfSkuById(sku.id);
        }
        else {
            this.linesInCart.push(
                {
                    price: sku.price,
                    quantity: 1,
                    sku: sku
                }
            )
        }
    }
    removeAllSkusFromCart() {
        this.linesInCart = [];
    }
    removeSkuFromCart(skuId: string, quantity?: number) {
        this.linesInCart = this.linesInCart.filter(lineInCart => lineInCart.sku.id !== skuId);
    }
    increaseQuantityOfSkuById(skuId: string, amount: number = 1) {
        const lineInCart = this.findLineInCartBySkuId(skuId);
        if (lineInCart)
            lineInCart.quantity += amount;
    }

    decreaseQuantityOfSkuById(skuId: string, amount: number = 1) {
        const lineInCart = this.findLineInCartBySkuId(skuId);
        if (lineInCart) {
            lineInCart.quantity -= amount;

            if (lineInCart.quantity < 1)
                this.removeSkuFromCart(skuId);
        }
    }
    getTotalPrice() {
        return this.linesInCart.reduce((currentSum: number, currentLineInCart: ProductLineInCart) => {
            return currency(currentSum).add(currentLineInCart.quantity * currentLineInCart.price).value;
        }, 0);
    }

    isSkuAlreadyInCart(skuId: string) {
        return this.linesInCart.find(lineInCart => lineInCart.sku.id === skuId) !== undefined;
    }

    findLineInCartBySkuId(skuId: string) {
        return this.linesInCart.find(lineInCart => lineInCart.sku.id === skuId);
    }

};