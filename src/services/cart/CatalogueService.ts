import { CatalogueInterface } from "../../types/catalogue/CatalogueInterface";
import { Product } from "../../types/product/Product";
import productsFromJson from '../../resources/product.json';
export class CatalogueService implements CatalogueInterface {
    
    productsList: Product[];
    constructor() {
        this.productsList = productsFromJson;
    }
    
    getProduct(idProduct: string): Product | undefined {
        return this.productsList.find(product => product.id === idProduct);
    }

}