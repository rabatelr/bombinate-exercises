import { CatalogueService } from "./CatalogueService";

describe('Catalogue creation', ()=> {
    test('catalogue product list contains products after service instanciation', ()=> {
        const catalogue = new CatalogueService();
        expect(catalogue.productsList).toHaveLength(5);
    })
});

describe('Catalogue product search by id ', ()=> {
    let catalogue: CatalogueService;
    beforeEach(()=> {
        catalogue = new CatalogueService();
    })
    test('catalogue product should be retrieved', ()=> {
        const product = catalogue.getProduct('2000001000414520178');
        expect(product?.brand.name).toBe('Michael Belhadi');
    })
    test('catalogue search with unknown idea should return undefined', ()=> {
        const product = catalogue.getProduct('unknownId');
        expect(product).toBeUndefined();
    });
});