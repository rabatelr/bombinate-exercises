import { CartService } from "./CartService";

beforeEach(() => {

})


describe('Cart creation', () => {


    test('Cart should be empty when created', () => {
        const cart = new CartService();
        expect(cart.linesInCart).toEqual([])
    });





})

describe('Removing items from the cart', () => {
    let cart: CartService;
    beforeEach(() => {
        cart = new CartService();
        cart.addSkuToCart(
            {
                description: 'testobject1',
                price: 25,
                stock: 4,
                id: 'idtestobject1'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
    })


    test('Cart should be empty after been cleared', () => {
        cart.removeAllSkusFromCart();
        expect(cart.linesInCart).toHaveLength(0);
    })

    describe('When item is removed', () => {

        test('should totally remove sku from cart even when current quantity is more than 1', () => {
            expect(cart.isSkuAlreadyInCart('idtestobject2')).toBeTruthy();
            cart.removeSkuFromCart('idtestobject2');
            expect(cart.isSkuAlreadyInCart('idtestobject2')).toBeFalsy();
        })
    });

    describe('When quantity is changed', () => {
        test('should increase sku quantity in cart by the correct amount', () => {
            expect(cart.findLineInCartBySkuId('idtestobject1')?.quantity).toEqual(1)
            cart.increaseQuantityOfSkuById('idtestobject1');
            expect(cart.findLineInCartBySkuId('idtestobject1')?.quantity).toEqual(2)
            cart.increaseQuantityOfSkuById('idtestobject1', 4);
            expect(cart.findLineInCartBySkuId('idtestobject1')?.quantity).toEqual(6)
        })

        test('should decrease sku quantity in cart by the correct amount', () => {
            expect(cart.findLineInCartBySkuId('idtestobject2')?.quantity).toEqual(6)
            cart.decreaseQuantityOfSkuById('idtestobject2');
            expect(cart.findLineInCartBySkuId('idtestobject2')?.quantity).toEqual(5)
            cart.decreaseQuantityOfSkuById('idtestobject2', 4);
            expect(cart.findLineInCartBySkuId('idtestobject2')?.quantity).toEqual(1)
        })
        test('should remove from cart items with quantity < 1', () => {
            expect(cart.findLineInCartBySkuId('idtestobject1')?.quantity).toEqual(1)
            cart.decreaseQuantityOfSkuById('idtestobject1');
            expect(cart.isSkuAlreadyInCart('idtestobject1')).toBeFalsy();

            expect(cart.findLineInCartBySkuId('idtestobject2')?.quantity).toEqual(6)
            cart.decreaseQuantityOfSkuById('idtestobject2', 10);
            expect(cart.isSkuAlreadyInCart('idtestobject2')).toBeFalsy()
        })
    });

});

describe('Calculating cart price', () => {
    test('should return the correct expected price', () => {
        const cart = new CartService();
        cart.addSkuToCart(
            {
                description: 'testobject1',
                price: 25,
                stock: 4,
                id: 'idtestobject1'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        expect(cart.getTotalPrice()).toBe(115)
    })
    test('when result is a float, should return the correct expected price without js floating point issues', () => {
        const cart = new CartService();
        cart.addSkuToCart(
            {
                description: 'testobject1',
                price: 25.60,
                stock: 4,
                id: 'idtestobject1'
            }
        )
        cart.addSkuToCart(
            {
                description: 'testobject2',
                price: 30.30,
                stock: 4,
                id: 'idtestobject2'
            }
        )
        expect(cart.getTotalPrice()).toBe(55.90)
    })
})