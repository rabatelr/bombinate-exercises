import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { ProductDetailsPage } from './pages/ProductDetailsPage';
import { CataloguePage } from './pages/CataloguePage';
import { Product } from './types/product/Product';
import { CatalogueService } from './services/cart/CatalogueService';
import { Cart } from './components/Cart';
import { CartService } from './services/cart/CartService';
import { ProductLineInCart } from './types/cart/CartInterface';
const catalogueService = new CatalogueService();
const cartService = new CartService();

function App() {
  const initialCatalogue: Product[] = [];
  const initialLinesInCart: ProductLineInCart[] = [];
  const [articles, setArticles] = useState(initialCatalogue);
  const [linesInCart, setLinesInCart] = useState(initialLinesInCart);
  useEffect(() => {
    let articles = catalogueService.productsList;
    setArticles(articles);
  }, []);

  return (
    <div className="d-flex">
      <div className="d-flex main-content col-8">
        <Router>
          <Route path="/" exact component={() => <CataloguePage catalogueService={catalogueService} />} />
          <Route path="/products/:id" component={() => <ProductDetailsPage updateCart={setLinesInCart} catalogueService={catalogueService} cartService={cartService} />} />
        </Router>
      </div>
      <div className="d-flex flex-column col-4 cart">
        <Cart linesInCartProps={linesInCart} updateCart={setLinesInCart} cartservice={cartService}/>
      </div>
    </div>
  );

}

export default App;