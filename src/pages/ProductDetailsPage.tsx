import * as React from 'react';
import { FunctionComponent } from 'react';
import {
    useParams
} from "react-router-dom";
import { ProductDetails } from '../components/ProductDetails';
import { CartService } from '../services/cart/CartService';
import { CatalogueInterface } from '../types/catalogue/CatalogueInterface';
import { ProductLineInCart } from '../types/cart/CartInterface';

export const ProductDetailsPage: FunctionComponent<ProductDetailsPageProps> = (props: ProductDetailsPageProps) => {
    const { id } = useParams();
    const { catalogueService, cartService, updateCart } = props;
    let product;
    if (id)
        product = catalogueService.getProduct(id);

    return <div>
        <ProductDetails product={product} cartService={cartService} updateCart={updateCart}/>
    </div>
}

export type ProductDetailsPageProps = {
    catalogueService: CatalogueInterface;
    cartService: CartService;
    updateCart: React.Dispatch<React.SetStateAction<ProductLineInCart[]>>;
}