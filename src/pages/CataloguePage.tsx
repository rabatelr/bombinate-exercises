import * as React from 'react';
import { FunctionComponent } from 'react';
import { CatalogueService } from '../services/cart/CatalogueService';
import { Link } from 'react-router-dom';
export const CataloguePage: FunctionComponent<CatalogueComponentProps> = (props: CatalogueComponentProps) => {

    const {catalogueService} = props;
    return <div className="d-flex col-12 row justify-content-center">
        {catalogueService.productsList.map((product, index) => {

            return <Link key={`product-${index}`} to={`/products/${product.id}`}>
                <div className="d-flex flex-column col-3 flex-wrap m-3">
                    <div className="name-brand text-center">
                        {product.brand.name}
                    </div>
                    <img src={product.image} className="col-6 text-center" />
                </div>
            </Link>
        })
        }
    </div>
}

export type CatalogueComponentProps = {
    catalogueService: CatalogueService;
}