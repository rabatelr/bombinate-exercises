import * as React from 'react';
import { FunctionComponent } from 'react';
import { Product, Sku } from '../types/product/Product';
import { Link } from 'react-router-dom';
import { CartService } from '../services/cart/CartService';
import { ProductLineInCart } from '../types/cart/CartInterface';

export const ProductDetails: FunctionComponent<ProductDetailsComponentProps> = (props: ProductDetailsComponentProps) => {
    const { product, cartService, updateCart } = props;

    const addToCartHandler = (sku: Sku) => {
        cartService.addSkuToCart(sku);
        updateCart([...cartService.linesInCart]);
    }

    return <div className="d-flex flex-column align-items-center">
        <div className="brand-name">
            {product?.brand.name}
        </div>
        {product?.skus.map((sku, index) => {
            return <div key={`sku-${index}`} className="d-flex flex-column align-items-center mt-3">
                <div className="description">
                    {sku.description}
                </div>
                <div className="price">
                    {sku.price}£
                </div>
                <button type="button" className="btn btn-primary" onClick={() => addToCartHandler(sku)}
                >Add to cart ! </button>

            </div>
        })}
        <Link to='/'>
            <button type="button" className="btn btn-warning mt-5">Go back to catalogue </button>
        </Link>
    </div>

}

export type ProductDetailsComponentProps = {
    product: Product | undefined;
    cartService: CartService;
    updateCart: React.Dispatch<React.SetStateAction<ProductLineInCart[]>>;
}