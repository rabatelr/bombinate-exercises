import * as React from 'react';
import { FunctionComponent, useState, useEffect } from 'react';
import { CartInterface, ProductLineInCart } from '../types/cart/CartInterface';
import { Sku } from '../types/product/Product';
export const Cart: FunctionComponent<CartComponentProps> = (props: CartComponentProps) => {
    const { linesInCartProps, cartservice, updateCart } = props;

    const clearCartHandler = () => {
        cartservice.removeAllSkusFromCart();
        updateCart([...cartservice.linesInCart]);
    }

    const increaseItemQuantityHandler = (sku: Sku) => {
        cartservice.increaseQuantityOfSkuById(sku.id);
        updateCart([...cartservice.linesInCart]);
    }

    const decreaseItemQuantityHandler = (sku: Sku) => {
        cartservice.decreaseQuantityOfSkuById(sku.id);
        updateCart([...cartservice.linesInCart]);
    }

    const removeItemHandler = (sku: Sku) => {
        cartservice.removeSkuFromCart(sku.id);
        updateCart([...cartservice.linesInCart]);
    }


    return linesInCartProps.length > 0 ? <div className="cart-container border p-3">
        {linesInCartProps.map(
            (line, index) => {
                return <div key={`line-in-cart-${index}`}className="d-flex align-items-center">
                    <div className="line-cart d-flex flex-column justify-content-center my-4">
                        <div className="description">{line.sku.description}</div>
                        <div className="price">{line.price}£</div>
                        <div className="d-flex container-amount align-items-center">
                            <div className="quantity">amount : {line.quantity}</div>
                            <button type="button" className="btn btn-outline-info mx-2" onClick={() => increaseItemQuantityHandler(line.sku)}>+</button>
                            <button type="button" className="btn btn-outline-info" onClick={() => decreaseItemQuantityHandler(line.sku)}>-</button>
                        </div>
                    </div>
                    <div className="remove-item-container ml-3">
                        <button type="button" className="btn btn-outline-danger" onClick={() => removeItemHandler(line.sku)}>Remove item</button>
                    </div>
                </div>
            }
        )}

        <div className="d-flex total-price-container">
            <div className="total-price">Total price : {cartservice.getTotalPrice()}</div>
        </div>

        <button onClick={clearCartHandler} type="button" className="btn btn-danger mt-2">Remove all items in cart</button>
        }

    </div>
        :
        <div>
            Add some items to the cart
        </div>
}




export type CartComponentProps = {
    linesInCartProps: ProductLineInCart[];
    cartservice: CartInterface;
    updateCart: React.Dispatch<React.SetStateAction<ProductLineInCart[]>>;
}