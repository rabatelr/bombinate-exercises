# README #

Hi Sunil, thank you for taking the time to review this code. 

## Main task ##

First of all, I'm sorry but the project is missing the e2e tests as I couldn't write them in time.
if you're still interested to see some, I could add them later as soon as I have time.
It could have been faster, but I took this exercise as an opportunity to learn to use react hooks and functional components (as it seems to have become the preferred react way at the moment) :). I was using class components before that.

# Refactor task # 

I put the files regarding the refactoring task in the same project in src->bombinate-refactor.
I would to warn you that it wasn't a strict refactor, I also added a return statement at the beginning of the function that I think was needed for the function to be properly used.

Thank you for your interest.